import 'package:flutter/material.dart';

class Radiolistile extends StatefulWidget {
  @override
  _RadiolistileState createState() => _RadiolistileState();
}

class _RadiolistileState extends State<Radiolistile> {
  var _result;

  @override
  Widget build(BuildContext context) {
    return Container(
        child: Padding(
            padding: EdgeInsets.all(45),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Text('1+2+3+4= ?'),
                RadioListTile(
                  title: Text('4'),
                  value: 4,
                  groupValue: _result,
                  onChanged: (value) {
                    setState(() {
                      _result = value;
                    });
                  },
                ),
                RadioListTile(
                    title: Text('5.4'),
                    value: 5.4,
                    groupValue: _result,
                    onChanged: (value) {
                      setState(() {
                        _result = value;
                      });
                    }),
                RadioListTile(
                    title: Text('6'),
                    value: 6,
                    groupValue: _result,
                    onChanged: (value) {
                      setState(() {
                        _result = value;
                      });
                    }),
                RadioListTile(
                  title: Text('7'),
                  value: 7,
                  groupValue: _result,
                  onChanged: (value) {
                    setState(() {
                      _result = value;
                    });
                  },
                ),
                SizedBox(height: 25),
                Text(_result == 7 ? 'correct' : 'not correct')
              ],
            )));
  }
}
