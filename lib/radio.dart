import 'package:flutter/material.dart';
import 'package:radio_button/radioListile.dart';

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  // The inital group value
  @override
  String _selectedGender = 'male';
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text(
          'Radio Button',
        ),
      ),
      body: Container(
        child: Padding(
          padding: EdgeInsets.all(65),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                child: Radiolistile(),
              ),
              Text('Please let us know your gender:'),
              ListTile(
                leading: Radio(
                  value: 'male',
                  groupValue: _selectedGender,
                  onChanged: (value) {
                    setState(() {
                      _selectedGender = value;
                    });
                  },
                ),
                title: Text('Male'),
              ),
              ListTile(
                leading: Radio(
                  value: 'female',
                  groupValue: _selectedGender,
                  onChanged: (value) {
                    setState(() {
                      _selectedGender = value;
                    });
                  },
                ),
                title: Text('Female'),
              ),
              SizedBox(height: 25),
              Text(_selectedGender == 'male' ? 'Hello gentlement!' : 'Hi lady!')
            ],
          ),
        ),
      ),
    );
  }
}
